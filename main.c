/*
 * Copyright (c) 2016, 2016 Glenn Wurr III <glenn@wurr.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */


#include <avr/io.h>
#include <util/delay.h>

// ====================================
//                ATtiny
//               25/45/85
//              +--------+
//            --+ o  Vcc +------------
//            --+        +--PB2- ADC IN to POT Wiper
//            --+        +--PB1- PWM OUT 
//  ------------+ GND    +--
//              +--------+
// ====================================

void adc_setup (void)
{
	// Set the ADC input to PB2/ADC1
	ADMUX |= (1 << MUX0);
	ADMUX |= (1 << ADLAR);
	
	// Set the prescaler to clock/128 & enable ADC
	ADCSRA |= (1 << ADPS1) | (1 << ADPS0) | (1 << ADEN);
}


int adc_read (void)
{
	// Start the conversion
	ADCSRA |= (1 << ADSC);
	
	// Wait for it to finish - blocking
	while (ADCSRA & (1 << ADSC));
	
	return ADCH;
}

void pwm_setup (void)
{
	// Set to 'Fast PWM' mode
	TCCR0A |= (1 << WGM01) | (1 << WGM00);

	// Clear OC0B output on compare match, upwards counting.
	TCCR0A |= (1 << COM0B1);

	// Prescaling
	//TCCR0B = _BV(CS00) ; //none
	//TCCR0B |= (1 << CS01); // clock/8 = 1mhz
	//TCCR0B |= (1 << CS01) | (1 << CS00); // clock/64 like 15khz
	//TCCR0B |= (1 << CS02); // clock/256 = like 4khz. lol cool strobe light effect
	TCCR0B |= (1 << CS02) | (1 << CS00); // clock/1024 wow thats fucking slow

	//default to off
	OCR0B = 0;

}

void pwm_write (int val)
{
	OCR0B = val;
}


int main(void)
{

	int adc_in;
	// Pin as output
        DDRB   |= (1 << PB1); 
	adc_setup();
	pwm_setup();

	while(1) {
		adc_in = adc_read();
		pwm_write(adc_in);
	}

	return(0);

}

